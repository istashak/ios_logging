//
//  AppDelegate.swift
//  LogglyLogger
//
//  Created by Ivan Stashak on 10/28/16.
//  Copyright © 2016 Cardinal Peak LLC. All rights reserved.
//

import UIKit

//import CocoaLumberjack
import LogglyLogger_CocoaLumberjack

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let logglyLogger: LogglyLogger = LogglyLogger();
        logglyLogger.logFormatter = LogglyFormatter();
        logglyLogger.logglyKey = "8512ef91-ebab-419a-847a-0b42e79d3543";
        
        logglyLogger.saveInterval = 15;
        
        let log: DDLog = DDLog.sharedInstance();
        log.add(logglyLogger, with: DDLogLevel.error);
        
        /*  This will add a pair of "loggers" to the logging framework. In other words, your log statements will be sent to the Console.app and the Xcode console (just like a normal NSLog). */
        log.add(DDASLLogger.sharedInstance(), with: DDLogLevel.verbose);
        log.add(DDTTYLogger.sharedInstance(), with: DDLogLevel.verbose);
        
        /* Lumberjack also has the ability to log to files.  The code below allows a weeks worth of rolling logs to be stored to file. */
        let fileLogger: DDFileLogger = DDFileLogger();
        fileLogger.rollingFrequency = 60 * 60 * 24; // 24 hour rolling
        fileLogger.logFileManager.maximumNumberOfLogFiles = 7;
        
        log.add(fileLogger, with: DDLogLevel.error);
        
        DDLogVerbose("{\"myLogglyKey\":\"Verbose log OOO\"}");
        DDLogError("{\"myLogglyKey\":\"Error log WWW\"}");
        
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

