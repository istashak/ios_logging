#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "LogglyFields.h"
#import "LogglyFormatter.h"
#import "LogglyLogger.h"

FOUNDATION_EXPORT double LogglyLogger_CocoaLumberjackVersionNumber;
FOUNDATION_EXPORT const unsigned char LogglyLogger_CocoaLumberjackVersionString[];

