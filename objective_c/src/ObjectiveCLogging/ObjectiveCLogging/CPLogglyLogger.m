//
//  LogglyLogger.m
//  ObjectiveCLogging
//
//  Created by Ivan Stashak on 11/10/16.
//  Copyright © 2016 Cardinal Peak LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CPLogglyLogger.h"

@implementation CPLogglyLogger

- (CPLogglyLogger*)init:(NSString*)logglyApiKey {
    LogglyLogger *logglyLogger = [[LogglyLogger alloc] init];
    [logglyLogger setLogFormatter:[[LogglyFormatter alloc] init]];
    
    /* The loggly customer token can be found at:
     https://<your_domain>.loggly.com/tokens */
    logglyLogger.logglyKey = logglyApiKey;
    
    /* Set posting interval every 15 seconds, just for testing this out, but the default value of 600 seconds is better in apps that normally don't access the network very often. When the user suspends the app, the logs will always be posted. */
    logglyLogger.saveInterval = 15;
    
    
    [DDLog addLogger:logglyLogger];
    
    return self;
}

- (void)d:(NSString*)tag withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    DDLogDebug([self formatMessage:message withParameters:args]);
    va_end(args);
}

- (void)d:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    DDLogDebug(@"%@ %@", [exception reason], [self formatMessage:message withParameters:args]);
    va_end(args);
}

- (void)e:(NSString*)tag withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    DDLogDebug([self formatMessage:message withParameters:args]);
    va_end(args);
}

- (void)e:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    DDLogError(@"%@ %@", [exception reason], [self formatMessage:message withParameters:args]);
    va_end(args);
}

- (void)i:(NSString*)tag withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    DDLogInfo([self formatMessage:message withParameters:args]);
    va_end(args);
}

- (void)i:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    DDLogInfo(@"%@ %@", [exception reason], [self formatMessage:message withParameters:args]);
    va_end(args);
}

- (void)v:(NSString*)tag withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    DDLogVerbose([self formatMessage:message withParameters:args]);
    va_end(args);
}

- (void)v:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    DDLogVerbose(@"%@ %@", [exception reason], [self formatMessage:message withParameters:args]);
    va_end(args);
}

- (void)w:(NSString*)tag withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    DDLogWarn([self formatMessage:message withParameters:args]);
    va_end(args);
}

- (void)w:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    DDLogWarn(@"%@ %@", [exception reason], [self formatMessage:message withParameters:args]);
    va_end(args);
}

@end
