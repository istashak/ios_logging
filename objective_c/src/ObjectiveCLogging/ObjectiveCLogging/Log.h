//
//  Log.h
//  ObjectiveCLogging
//
//  Created by Ivan Stashak on 11/8/16.
//  Copyright © 2016 Cardinal Peak LLC. All rights reserved.
//


#ifndef Log_h
#define Log_h


typedef NS_ENUM(NSInteger, LogLevel) {
    
    VERBOSE_LEVEL = 1,
    DBUG_LEVEL    = 2,
    INFO_LEVEL    = 4,
    WARNING_LEVEL = 8,
    ERROR_LEVEL   = 16
    
};

//@interface ILogger : NSObject
//
//- (void)d:(NSString*)tag withMessage:(NSString*)message, ...;
//- (void)d:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
//- (void)e:(NSString*)tag withMessage:(NSString*)message, ...;
//- (void)e:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
//- (void)i:(NSString*)tag withMessage:(NSString*)message, ...;
//- (void)i:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
//- (void)v:(NSString*)tag withMessage:(NSString*)message, ...;
//- (void)v:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
//- (void)w:(NSString*)tag withMessage:(NSString*)message, ...;
//- (void)w:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
//
//- (void)log:(LogLevel)level withTag:(NSString*)tag withException:(NSException*)exception withMessage:message, ...;
//- (void)log:(LogLevel)level withTag:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message withParameters:(va_list)valist;
//
//@end

@interface AbstractLogger : NSObject

- (NSString*) formatMessage:(NSString*)message withParameters:(va_list)valist;

- (void)d:(NSString*)tag withMessage:(NSString*)message, ...;
- (void)d:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
- (void)e:(NSString*)tag withMessage:(NSString*)message, ...;
- (void)e:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
- (void)i:(NSString*)tag withMessage:(NSString*)message, ...;
- (void)i:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
- (void)v:(NSString*)tag withMessage:(NSString*)message, ...;
- (void)v:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
- (void)w:(NSString*)tag withMessage:(NSString*)message, ...;
- (void)w:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;

- (void)log:(LogLevel)level withTag:(NSString*)tag withException:(NSException*)exception withMessage:message, ...;
- (void)log:(LogLevel)level withTag:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message withParameters:(va_list)valist;

@end

@interface Log : NSObject

+ (void)initialize;

+ (void)d:(NSString*)tag withMessage:(NSString*)message, ...;
+ (void)d:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
+ (void)e:(NSString*)tag withMessage:(NSString*)message, ...;
+ (void)e:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
+ (void)i:(NSString*)tag withMessage:(NSString*)message, ...;
+ (void)i:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
+ (void)v:(NSString*)tag withMessage:(NSString*)message, ...;
+ (void)v:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
+ (void)w:(NSString*)tag withMessage:(NSString*)message, ...;
+ (void)w:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;

//+ (void)log:(LogLevel)level withTag:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message withParameters:(va_list)valist;

//+ (void)addLogger:(ILogger*)logger;
+ (void)addLogger:(AbstractLogger*)logger;

@end

#endif /* Log_h */
