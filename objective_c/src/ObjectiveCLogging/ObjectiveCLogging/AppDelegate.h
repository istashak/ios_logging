//
//  AppDelegate.h
//  ObjectiveCLogging
//
//  Created by Ivan Stashak on 10/24/16.
//  Copyright © 2016 Cardinal Peak LLC. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

