//
//  ViewController.h
//  ObjectiveCLogging
//
//  Created by Ivan Stashak on 10/24/16.
//  Copyright © 2016 Cardinal Peak LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, assign) NSInteger clickCount;

@property (weak, nonatomic) IBOutlet UIButton *_requestButton;

@end

