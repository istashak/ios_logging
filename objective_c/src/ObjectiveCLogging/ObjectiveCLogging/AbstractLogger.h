//
//  AbstractLogger.h
//  ObjectiveCLogging
//
//  Created by Ivan Stashak on 11/10/16.
//  Copyright © 2016 Cardinal Peak LLC. All rights reserved.
//

#ifndef AbstractLogger_h
#define AbstractLogger_h

#import "Log.h"
#import <CocoaLumberjack/CocoaLumberjack.h>


//@interface AbstractLogger : ILogger
//
//- (NSString*) formatMessage:(NSString*)message withParameters:(va_list)valist;
//
//- (void)d:(NSString*)tag withMessage:(NSString*)message, ...;
//- (void)d:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
//- (void)e:(NSString*)tag withMessage:(NSString*)message, ...;
//- (void)e:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
//- (void)i:(NSString*)tag withMessage:(NSString*)message, ...;
//- (void)i:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
//- (void)v:(NSString*)tag withMessage:(NSString*)message, ...;
//- (void)v:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
//- (void)w:(NSString*)tag withMessage:(NSString*)message, ...;
//- (void)w:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
//
//- (void)log:(LogLevel)level withTag:(NSString*)tag withException:(NSException*)exception withMessage:message, ...;
//- (void)log:(LogLevel)level withTag:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message withParameters:(va_list)valist;
//
//@end

#endif /* AbstractLogger_h */
