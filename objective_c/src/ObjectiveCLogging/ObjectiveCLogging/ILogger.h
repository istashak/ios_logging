//
//  ILogger.h
//  ObjectiveCLogging
//
//  Created by Ivan Stashak on 11/9/16.
//  Copyright © 2016 Cardinal Peak LLC. All rights reserved.
//

#ifndef ILogger_h
#define ILogger_h

#import "Log.h"

//@protocol ILogger
//
//- (void)d:(NSString*)tag withMessage:(NSString*)message, ...;
//- (void)d:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
//- (void)e:(NSString*)tag withMessage:(NSString*)message, ...;
//- (void)e:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
//- (void)i:(NSString*)tag withMessage:(NSString*)message, ...;
//- (void)i:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
//- (void)v:(NSString*)tag withMessage:(NSString*)message, ...;
//- (void)v:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
//- (void)w:(NSString*)tag withMessage:(NSString*)message, ...;
//- (void)w:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ...;
//
//- (void)log:(LogLevel)level withTag:(NSString*)tag withException:(NSException*)exception withMessage:message, ...;
//
//@end

#endif /* ILogger_h */
