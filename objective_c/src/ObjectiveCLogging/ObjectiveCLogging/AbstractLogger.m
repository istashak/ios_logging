//
//  AbstractLogger.m
//  ObjectiveCLogging
//
//  Created by Ivan Stashak on 11/10/16.
//  Copyright © 2016 Cardinal Peak LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbstractLogger.h"
#import "Log.h"

@implementation AbstractLogger

- (NSString*) formatMessage:(NSString*)message withParameters:(va_list)valist {
    return [[NSString alloc]initWithFormat:message arguments:valist];
}

- (void)log:(LogLevel)level withTag:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message withParameters:(va_list)valist  {
    
    if(exception != nil) {
        
        if(message == nil || [message length] == 0) {
            message = @"";
            //args = new Object[] {};
        }
        
        message = [self formatMessage:message withParameters:valist];
        
        switch (level) {
                
            case DBUG_LEVEL:
                [self d:tag withException:exception withMessage:message];
                break;
                
            case ERROR_LEVEL:
                [self e:tag withException:exception withMessage:message];
                break;
                
            case INFO_LEVEL:
                [self i:tag withException:exception withMessage:message];
                break;
                
            case VERBOSE_LEVEL:
                [self v:tag withException:exception withMessage:message];
                break;
                
            case WARNING_LEVEL:
                [self w:tag withException:exception withMessage:message];
                break;
        }
    } else {
        
        // If there's nothing to log just return.
        if(message == nil || [message length] == 0) {
            return;
        }
        
        message = [self formatMessage:message withParameters:valist];
        
        switch (level) {
                
            case DBUG_LEVEL:
                [self d:tag withMessage:message];
                break;
                
            case ERROR_LEVEL:
                [self e:tag withMessage:message];
                break;
                
            case INFO_LEVEL:
                [self i:tag withMessage:message];
                break;
                
            case VERBOSE_LEVEL:
                [self v:tag withMessage:message];
                break;
                
            case WARNING_LEVEL:
                [self w:tag withMessage:message];
                break;
        }
    }
    
}

    

@end
