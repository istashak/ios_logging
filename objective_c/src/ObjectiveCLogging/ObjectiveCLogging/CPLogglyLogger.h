//
//  LogglyLogger.h
//  ObjectiveCLogging
//
//  Created by Ivan Stashak on 11/10/16.
//  Copyright © 2016 Cardinal Peak LLC. All rights reserved.
//

#ifndef CPLogglyLogger_h
#define CPLogglyLogger_h

#import "AbstractLogger.h"
#import "LogglyLogger.h"
#import "LogglyFormatter.h"
#import "Log.h"

//static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@interface CPLogglyLogger : AbstractLogger

- (CPLogglyLogger*)init:(NSString*)logglyApiKey;

@end

#endif /* LogglyLogger_h */
