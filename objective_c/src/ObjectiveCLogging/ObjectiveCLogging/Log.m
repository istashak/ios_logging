//
//  Log.m
//  ObjectiveCLogging
//
//  Created by Ivan Stashak on 11/8/16.
//  Copyright © 2016 Cardinal Peak LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Log.h"

//#define LOG(level, message, args) \
//        va_list args;              \
//        va_start(args, message);   \
//        [self log:level withTag:tag withException:nil withMessage:message, withParameters:args]; \
//        va_end(args) \

@implementation Log

//static NSMutableArray<ILogger*>* LOGGERS; // = [NSMutableArray arrayWithCapacity:10];
static NSMutableArray<AbstractLogger*>* LOGGERS;

+ (void)initialize {
    LOGGERS = [[NSMutableArray alloc] init];
}

+ (void)addLogger:(AbstractLogger*)logger {
    [LOGGERS addObject:logger];
}

+ (void)d:(NSString*)tag withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    [self log:DBUG_LEVEL withTag:tag withException:nil withMessage:message withParameters:args];
    va_end(args);
    
    //LOG(DEBUG, message, ...);
}

+ (void)d:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    [self log:DBUG_LEVEL withTag:tag withException:exception withMessage:message withParameters:args];
    va_end(args);
}

+ (void)e:(NSString*)tag withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    [self log:ERROR_LEVEL withTag:tag withException:nil withMessage:message withParameters:args];
    va_end(args);
}

+ (void)e:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    [self log:ERROR_LEVEL withTag:tag withException:exception withMessage:message withParameters:args];
    va_end(args);
}

+ (void)i:(NSString*)tag withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    [self log:INFO_LEVEL withTag:tag withException:nil withMessage:message withParameters:args];
    va_end(args);
}

+ (void)i:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    [self log:INFO_LEVEL withTag:tag withException:exception withMessage:message withParameters:args];
    va_end(args);
}

+ (void)v:(NSString*)tag withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    [self log:VERBOSE_LEVEL withTag:tag withException:nil withMessage:message withParameters:args];
    va_end(args);
}

+ (void)v:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    [self log:VERBOSE_LEVEL withTag:tag withException:exception withMessage:message withParameters:args];
    va_end(args);
}

+ (void)w:(NSString*)tag withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    [self log:WARNING_LEVEL withTag:tag withException:nil withMessage:message withParameters:args];
    va_end(args);
}

+ (void)w:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message, ... {
    va_list args;
    va_start(args, message);
    [self log:WARNING_LEVEL withTag:tag withException:exception withMessage:message withParameters:args];
    va_end(args);
}

+ (void)log:(LogLevel)level withTag:(NSString*)tag withException:(NSException*)exception withMessage:(NSString*)message withParameters:(va_list)valist  {
    @synchronized(self){
        for(int i = 0; [LOGGERS count]; ++i) {
            //ILogger* logger = [LOGGERS objectAtIndex:i];
            AbstractLogger* logger = [LOGGERS objectAtIndex:i];
            [logger log:level withTag:tag withException:exception withMessage:message withParameters:valist];
        }
        
        //NSLogv(message, valist);
    }
}

@end
