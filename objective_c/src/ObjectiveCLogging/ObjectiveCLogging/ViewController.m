//
//  ViewController.m
//  ObjectiveCLogging
//
//  Created by Ivan Stashak on 10/24/16.
//  Copyright © 2016 Cardinal Peak LLC. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _clickCount = 0;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)handleRequestButtonClick:(id)sender {
    DDLogVerbose(@"Click count = %d", ++_clickCount);
}


@end
