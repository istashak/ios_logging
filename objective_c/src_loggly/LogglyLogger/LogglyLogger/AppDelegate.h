//
//  AppDelegate.h
//  LogglyLogger
//
//  Created by Ivan Stashak on 11/2/16.
//  Copyright © 2016 Cardinal Peak LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LogglyLogger.h"
#import "LogglyFormatter.h"

// This sets the log level
static const DDLogLevel ddLogLevel = DDLogLevelVerbose;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

