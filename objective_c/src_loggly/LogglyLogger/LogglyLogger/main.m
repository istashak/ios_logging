//
//  main.m
//  LogglyLogger
//
//  Created by Ivan Stashak on 11/2/16.
//  Copyright © 2016 Cardinal Peak LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
